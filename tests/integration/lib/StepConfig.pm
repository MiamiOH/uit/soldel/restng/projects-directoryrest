#!perl

package StepConfig;
use strict;
use warnings;

use Data::Dumper;
use DBI;

use Exporter;
our @ISA = 'Exporter';
our @EXPORT = qw(
                    $server
                    $resourcePaths
                    getResourcePath
                    $authCredentials
              );

our $server = 'http://ws/api';

our $resourcePaths = {
    'authentication' => '/authentication/v1',
};

sub getResourcePath {
    my $pathKey = shift;
    my $values = shift;

    my $path = '';

    if (defined($resourcePaths->{$pathKey})) {
        $path = $resourcePaths->{$pathKey};

        if ($values && ref($values) eq 'HASH') {
            foreach my $key ( keys %{$values}) {
                $path =~ s/\{$key\}/$values->{$key}/g;
            }
        }
    }

    return $path;
}
our $authCredentials = {
    'idVault' => {
                     'username' => '_WS_USER',
                     'password' => 'secr3t',
                 },
};

1;