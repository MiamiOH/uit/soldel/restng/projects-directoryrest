<?php


namespace MiamiOH\ProjectsDirectoryRest\Tests\Unit;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\Response;

use MiamiOH\RESTng\Exception\ResourceNotFound;

class directoryEntryTest extends \MiamiOH\RESTng\Testing\TestCase
{
    private $request, $dbh, $entry,$mockApp;
    protected function setUp()
    {
        $this->mockApp = $this->createMock(App::class);
        $this->mockApp->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array(
                'getResourceParam','getData'
            ))
            ->getMock();

        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(['queryfirstrow_assoc','perform','queryfirstcolumn'])
            ->getMock();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(['getHandle'])
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $this->entry = new \MiamiOH\ProjectsDirectoryRest\Services\Entry();

        $this->entry->setApp($this->mockApp);
        $this->entry->setDatabase($db);
        $this->entry->setRequest($this->request);
    }

    public function testGetAccountDetailsForManualAccount()
    {
        $this->request->method('getResourceParam')
            ->will($this->returnCallback(array($this, 'mockUniqueId')));

        $this->dbh->expects($this->exactly(2))->method('queryfirstcolumn')
            ->willReturn(1);

        $this->dbh->expects($this->once())->method('queryfirstrow_assoc')
            ->will($this->returnCallback(array(
                $this,
                'mockManualAccount'
            )));


        $this->response = $this->entry->getEntryAccountDetails();
        $payload = $this->response->getPayload();
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, [
            'id'=>'',
            'uniqueId' => 'doej',
            'type' => 'managed',
            'firstName' => 'John',
            'lastName' => 'Doe'
        ]);
    }

    public function testExpectNotFoundException()
    {
        $this->request->method('getResourceParam')
            ->will($this->returnCallback(array($this, 'mockUniqueId')));

        $this->dbh->expects($this->atLeastOnce())->method('queryfirstcolumn')
            ->willReturn(0);
        $this->expectException(\MiamiOH\RESTng\Exception\ResourceNotFound::class);
        $this->response = $this->entry->getEntryAccountDetails();
    }

    public function testGetDetailsForEntityAccount()
    {
        $resourceResponse = new Response();
        $resourceResponse->setStatus(App::API_CREATED);
        $resourceResponse->setPayload([
            'firstName' => 'John',
            'lastName' => 'Doe',
        ]);

        $this->mockApp->method('callResource')
            ->with(
                $this->equalTo('directory.v1.query.lookup')
            )
            ->willReturn($resourceResponse);

        $this->request->method('getResourceParam')
            ->will($this->returnCallback(array($this, 'mockUniqueId')));

        $this->dbh->expects($this->exactly(3))->method('queryfirstcolumn')
            ->willReturnOnConsecutiveCalls(1, 0, 1);
        $this->dbh->expects($this->once())->method('queryfirstrow_assoc')
            ->will($this->returnCallback(array(
                $this,
                'mockEntityValues'
            )));

        $this->response = $this->entry->getEntryAccountDetails();
        $payload = $this->response->getPayload();

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload['type'], 'entity');
        $this->assertEquals($payload['firstName'], 'John');
        $this->assertEquals($payload['lastName'], 'Doe');
    }

    public function testGetAccountDetailsForFamilyAccount()
    {
        $this->request->method('getResourceParam')
            ->will($this->returnCallback(array($this, 'mockUniqueId')));

        $this->dbh->expects($this->exactly(4))->method('queryfirstcolumn')
            ->willReturnOnConsecutiveCalls(1, 0, 0, 1);

        $this->dbh->expects($this->once())->method('queryfirstrow_assoc')
            ->will($this->returnCallback(array(
                $this,
                'mockFamilyAccountValues'
            )));

        $this->response = $this->entry->getEntryAccountDetails();
        $payload = $this->response->getPayload();
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, [
            'id'=>'111',
            'uniqueId' => 'doej',
            'type' => 'family',
            'firstName' => 'John',
            'lastName' => 'Doe'
        ]);
    }

    public function mockUniqueId()
    {
        return 'doej';
    }

    public function mockEmptyUniqueId()
    {
        return '';
    }

    public function mockManualAccount()
    {
        return [
            'id'=>'',
            'uniqueid' => 'doej',
            'type'=>'managed',
            'firstname' => 'John',
            'lastname' => 'Doe'
        ];
    }
    public function mockFamilyAccountValues()
    {
        return [
            'id'=>'111',
            'uniqueid' => 'doej',
            'type'=>'family',
            'firstname' => 'John',
            'lastname' => 'Doe'
        ];
    }

    public function mockEntityValues()
    {
        return [
            'uniqueid' => 'doej',
            'entity_type' => 'a'
        ];
    }
    public function mockCreateDataset(){
        return [
            'id' => 1234,
            'uniqueId' => 'testing@test.com',
            'type' => 'family',
            'firstName' => 'testFirst',
            'lastName' => 'testLast'
        ];
    }

    public function mockBadDatasetforCreate(){
        return [
            'id' => 1234,
            'uniqueId' => 'testing',
            'type' => 'family',
            'firstName' => 'testFirst',
            'lastName' => 'testLast'
        ];
    }

    public function mockEmptyDataset(){
        return [];
    }

    public function testCreateNewEntry()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array($this, 'mockCreateDataset')));

        $this->dbh->method('queryfirstcolumn')
            ->will($this->onConsecutiveCalls('0','1'));

        $this->response = $this->entry->createEntry();
        $this->assertEquals(\MiamiOH\RESTng\App::API_CREATED, $this->response->getStatus());
        $this->assertEquals($this->response->getPayload(), [[
            'id' => 1234,
            'uniqueId' => 'testing@test.com',
            'type' => 'family',
            'firstName' => 'testFirst',
            'lastName' => 'testLast']]);
    }

    public function testCannotCreateForExsistingEntry()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array($this, 'mockCreateDataset')));

        $this->dbh->expects($this->once())->method('queryfirstcolumn')
            ->willReturn('1');

        $this->expectException('MiamiOH\RESTng\Exception\BadRequest');
        $this->response = $this->entry->createEntry();

    }

    public function testCannotCreateEntryForEmptyDataSet()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array($this, 'mockEmptyDataset')));

        $this->response = $this->entry->createEntry();
        $this->assertEquals(\MiamiOH\RESTng\App::API_BADREQUEST, $this->response->getStatus());
        $this->assertEquals($this->response->getPayload(), ['errors'=> ["No Data"]]);

    }

    public function testCannotCreateEntryForBadDataSet()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array($this, 'mockBadDatasetforCreate')));

        $this->dbh->method('queryfirstcolumn')
            ->will($this->onConsecutiveCalls('0','1'));

        $this->expectException('MiamiOH\RESTng\Exception\BadRequest');
        $this->response = $this->entry->createEntry();

    }

    public function emptyUniqueId()
    {
        return [
            'uniqueId' => ''
        ];
    }

    public function mockUniqueIdDataSet()
    {
        return  'testing';
    }

    public function noServiceDataSet()
    {
        return [
            'uniqueId' => 'testing'
        ];
    }

    public function testCannotHaveEmptyUniqueID()
    {
        $this->request->expects($this->once())->method('getResourceParam')
            ->will($this->returnCallback(array($this, 'mockUniqueIdDataSet')));
        $this->request->expects($this->once())->method('getData')
            ->will($this->returnCallback(array($this, 'emptyUniqueId')));

        $this->expectException('MiamiOH\RESTng\Exception\BadRequest');
        $this->response = $this->entry->updateEntry();

    }

    public function testNoServiceCanUpdateAll()
    {
        $this->request->expects($this->once())->method('getResourceParam')
            ->will($this->returnCallback(array($this, 'mockUniqueIdDataSet')));
        $this->request->method('getData')
            ->will($this->returnCallback(array($this, 'noServiceDataSet')));

        $this->dbh->expects($this->once())->method('queryfirstcolumn')
            ->willReturn(2323);

        $this->response = $this->entry->updateEntry();
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $this->response->getStatus());

    }

    public function serviceWithNoStatusDataSet()
    {
        return [
            'uniqueId' => 'testing',
            'services' => [
                [
                    'name' => 'GROUPS'
                ]
            ]
        ];
    }

    public function testWithServicesNoStatus()
    {
        $this->request->expects($this->once())->method('getResourceParam')
            ->will($this->returnCallback(array($this, 'mockUniqueIdDataSet')));
        $this->request->method('getData')
            ->will($this->returnCallback(array($this, 'serviceWithNoStatusDataSet')));

        $this->dbh->expects($this->once())->method('queryfirstcolumn')
            ->willReturn(2323);

        $this->response = $this->entry->updateEntry();
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $this->response->getStatus());

    }

    public function serviceWithInvalidStatusDataSet()
    {
        return [
            'uniqueId' => 'testing',
            'services' => [
                [
                    'name' => 'GROUPS',
                    'status' => 'Y'
                ]
            ]
        ];
    }

    public function testServicesWithInvalidStatus()
    {
        $this->request->expects($this->once())->method('getResourceParam')
            ->will($this->returnCallback(array($this, 'mockUniqueIdDataSet')));
        $this->request->method('getData')
            ->will($this->returnCallback(array($this, 'serviceWithInvalidStatusDataSet')));

        $this->dbh->expects($this->once())->method('queryfirstcolumn')
            ->willReturn(2323);

        $this->expectException('MiamiOH\RESTng\Exception\BadRequest');
        $this->response = $this->entry->updateEntry();

    }

}