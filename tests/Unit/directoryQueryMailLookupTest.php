<?php


class DirectoryQueryMailLookupTest extends \MiamiOH\RESTng\Testing\TestCase
{
    private $query;

    private $api;
    private $ldapFactoryObj;
    private $ldapObj;
    private $apiUser;

    protected function setup()
    {

        $this->api = $this->createMock(\MiamiOH\RESTng\App::class);

        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $this->apiUser = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthorized'))
            ->getMock();

        $this->apiUser->method('isAuthorized')->willReturn(1);

        $this->ldapFactoryObj = $this->getMockBuilder('\MiamiOH\RESTng\Connector\LDAPFactory')
            ->setMethods(array('getHandle'))
            ->getMock();

        $this->ldapObj = $this->getMockBuilder('\Dreamscapes\Ldap\Core\Ldap')
            ->setMethods(array('search'))
            ->getMock();

        $this->ldapResultObj = $this->getMockBuilder('\Dreamscapes\Ldap\Core\Result')
            ->disableOriginalConstructor()
            ->setMethods(array('getEntries', 'freeResult'))
            ->getMock();

        $this->query = new \MiamiOH\ProjectsDirectoryRest\Services\Query();

        $this->query->setApp($this->api);
        $this->query->setLdapFactory($this->ldapFactoryObj);
        $this->query->setApiUser($this->apiUser);

    }

    public function testQueryMailLookup()
    {

        $uid = 'testEntity';
        $firstName = 'John';
        $lastName = 'Doe';
        $affiliation = 'sta';
        $mailAlternateAddress = 'john.doe@miamioh.edu';

        $ldapResultData = array (
            'count' => 1,
            0 =>
                array (
                    'uid' =>
                        array (
                            'count' => 1,
                            0 => $uid,
                        ),
                    0 => 'uid',
                    'sn' =>
                        array (
                            'count' => 1,
                            0 => $lastName,
                        ),
                    1 => 'sn',
                    'givenname' =>
                        array (
                            'count' => 1,
                            0 => $firstName,
                        ),
                    2 => 'givenname',
                    'muohioeduprimaryaffiliationcode' =>
                        array (
                            'count' => 1,
                            0 => $affiliation,
                        ),
                    3 => 'muohioeduprimaryaffiliationcode',
                    'displayname' =>
                        array (
                            'count' => 1,
                            0 => $firstName . ' ' . $lastName,
                        ),
                    4 => 'displayname',
                    'mailAlternateAddress' =>
                        array (
                            'count' => 1,
                            0 => $mailAlternateAddress
                        ),
                    5 => 'displayname',
                    'muohioeduhidden' =>
                        array (
                            'count' => 1,
                            0 => 'N',
                        ),
                    6 => 'muohioeduhidden',
                    'count' => 7,
                    'dn' => 'uid=' . $uid . ',ou=people,dc=muohio,dc=edu',
                ),
        );

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

        $request->expects($this->once())->method('getOptions')
            ->willReturn(array('q' => $mailAlternateAddress));

        $this->ldapFactoryObj->expects($this->once())->method('getHandle')->willReturn($this->ldapObj);
        $this->ldapObj->expects($this->once())->method('search')->willReturn($this->ldapResultObj);
        $this->ldapResultObj->method('getEntries')->willReturn($ldapResultData);

        $this->query->setRequest($request);

        $resp = $this->query->mailLookup();

        $this->assertEquals(MiamiOH\RESTng\App::API_OK, $resp->getStatus());

        $payload = $resp->getPayload();

        $this->assertEquals(8, count(array_keys($payload)));
        $this->assertTrue(array_key_exists('firstName', $payload));
        $this->assertTrue(array_key_exists('lastName', $payload));
        $this->assertTrue(array_key_exists('uid', $payload));
        $this->assertTrue(array_key_exists('fullName', $payload));
        $this->assertTrue(array_key_exists('fullNameReverse', $payload));
        $this->assertTrue(array_key_exists('affiliation', $payload));
        $this->assertTrue(array_key_exists('hiddenStatus', $payload));
        $this->assertEquals($uid, $payload['uid']);
        $this->assertEquals($firstName, $payload['firstName']);
        $this->assertEquals($lastName, $payload['lastName']);
        $this->assertEquals($firstName . ' ' . $lastName, $payload['fullName']);
        $this->assertEquals($lastName . ', ' . $firstName, $payload['fullNameReverse']);
        $this->assertEquals($affiliation, $payload['affiliation']);
        $this->assertEquals('N', $payload['hiddenStatus']);

    }

    public function testQueryLookupNotFound()
    {

        $mailAlternateAddress = 'doesnotexist@miamioh.edu';

        $ldapResultData = array (
            'count' => 0
        );

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

        $request->expects($this->once())->method('getOptions')
            ->willReturn(array('q' => $mailAlternateAddress));

        $this->ldapFactoryObj->expects($this->once())->method('getHandle')->willReturn($this->ldapObj);
        $this->ldapObj->expects($this->once())->method('search')->willReturn($this->ldapResultObj);
        $this->ldapResultObj->method('getEntries')->willReturn($ldapResultData);

        $this->query->setRequest($request);

        $resp = $this->query->mailLookup();

        $this->assertEquals(MiamiOH\RESTng\App::API_NOTFOUND, $resp->getStatus());

    }

}
