<?php

use MiamiOH\RESTng\App;

class directoryMailableTest extends \MiamiOH\RESTng\Testing\TestCase
{
    private $request, $dbh,$mailableAccount;
    protected function setUp()
    {
        $mockApp = $this->createMock(App::class);
        $mockApp->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array(
                'getResourceParam','getData'
            ))
            ->getMock();

        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(['queryfirstrow_assoc','perform', 'queryfirstcolumn'])
            ->getMock();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(['getHandle'])
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $this->mailableAccount = new \MiamiOH\ProjectsDirectoryRest\Services\Mailable();

        $this->mailableAccount->setApp($mockApp);
        $this->mailableAccount->setDatabase($db);
        $this->mailableAccount->setRequest($this->request);
    }


    public function testGetMailable()
    {
        $this->request->method('getResourceParam')
            ->will($this->returnCallback(array($this, 'mockOptionsEntityAccountID')));

        $this->dbh->expects($this->once())->method('queryfirstcolumn')
            ->willReturn(1);

        $this->dbh->expects($this->once())->method('queryfirstrow_assoc')
            ->will($this->returnCallback(array(
                $this,
                'mockMailableAccount'
            )));

        $this->response = $this->mailableAccount->getMailable();
        $payload = $this->response->getPayload();
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->resultMailable());
    }

    public function mockOptionsEntityAccountID()
    {
        return 'Testing';
    }

    public function resultMailable()
    {
        return array(
                'id' => 'TESTING',
                'longName' => 'testing',
                'owner' => 'ANGELCL',
                'status' => 'a'
        );
    }

    public function mockMailableAccount()
    {
        return array(
                'gzbeatt_entity_id' => 'TESTING',
                'gzbeatt_long_name' => 'testing',
                'gzbeatt_owner_id' => 'ANGELCL',
                'gzbeatt_status' => 'a'
        );
    }

    public function mockCreateDataset(){
       return [
            'id' => 'TESTING',
            'longName' => 'test long',
            'owner' => 'kandasm',
            'status' =>'a'];
    }
    public function mockUpdateDataset(){
        return [
            'longName' => 'test long',
            'owner' => 'kandasm',
            'status' =>'a'];
    }
    public function mockBadDatasetforUpdate(){
        return [
            'longName' => 'test long',
            'owner' => 'test owner',
            'status' =>'longstatus'];
    }
    public function mockBadDataset(){
        return [
            'id' => 'TESTING',
            'longName' => 'test long',
            'owner' => 'test owner',
            'status' =>'longstatus'];
    }
    public function mockEmptyDataset(){
        return [];
    }
    public function testCreateMailableAccount()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array($this, 'mockCreateDataset')));

        $this->dbh->expects($this->once())->method('queryfirstcolumn')
            ->willReturn(0);

        $this->response = $this->mailableAccount->createMailable();
        $this->assertEquals(\MiamiOH\RESTng\App::API_CREATED, $this->response->getStatus());
        $this->assertEquals($this->response->getPayload(), [[
            'id' => 'TESTING',
            'longName' => 'test long',
            'owner' => 'kandasm',
            'status' => 'a']]);
    }
    public function testFailedToCreateMailableAccountWithBadOwner()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array($this, 'mockBadDataset')));

        $this->expectException('MiamiOH\RESTng\Exception\BadRequest');
        $this->response = $this->mailableAccount->createMailable();
    }

    public function testThrowExceptionForCreatingNewMailableAccount()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array($this, 'mockCreateDataset')));

        $this->dbh->expects($this->once())->method('queryfirstcolumn')
            ->willReturn(1);

        $this->expectException('MiamiOH\RESTng\Exception\BadRequest');
        $this->response = $this->mailableAccount->createMailable();
    }

    public function testFailedToCreateMailable()
    {
        $this->request->method('getResourceParam')
            ->will($this->returnCallback(array($this, 'mockOptionsEntityAccountID')));

        $this->request->method('getData')
            ->will($this->returnCallback(array($this, 'mockEmptyDataset')));
        $this->response = $this->mailableAccount->createMailable();

        $this->assertEquals(\MiamiOH\RESTng\App::API_BADREQUEST, $this->response->getStatus());
        $this->assertEquals($this->response->getPayload(), ['errors'=> ["No Data"]]);
    }
    public function mockDeleteDataset(){
        return [
            'id' => 'TESTING'];
    }
    public function emptyAccount(){
        return [];
    }
    public function testUpdateMailable()
    {
        $this->request->method('getResourceParam')
            ->will($this->returnCallback(array($this, 'mockOptionsEntityAccountID')));

        $this->request->method('getData')
            ->will($this->returnCallback(array($this, 'mockCreateDataset')));

        $this->dbh->expects($this->once())->method('queryfirstcolumn')
            ->willReturn(1);

        $this->response = $this->mailableAccount->updateMailable();
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $this->response->getStatus());
    }

    public function testFailedToUpdateMailable()
    {
        $this->request->method('getResourceParam')
            ->will($this->returnCallback(array($this, 'mockOptionsEntityAccountID')));

        $this->request->method('getData')
            ->will($this->returnCallback(array($this, 'mockEmptyDataset')));
        $this->response = $this->mailableAccount->updateMailable();

        $this->assertEquals(\MiamiOH\RESTng\App::API_BADREQUEST, $this->response->getStatus());
        $this->assertEquals($this->response->getPayload(), ['errors'=> ["No Data"]]);
    }

    public function testFailedToUpdateMailableAccountWithBadOwnerData()
    {
        $this->request->method('getResourceParam')
            ->will($this->returnCallback(array($this, 'mockOptionsEntityAccountID')));

        $this->request->method('getData')
            ->will($this->returnCallback(array($this, 'mockBadDatasetForUpdate')));

        $this->expectException('MiamiOH\RESTng\Exception\BadRequest');
        $this->response = $this->mailableAccount->updateMailable();
    }

    public function testUpdateNotExistMailableAccountThrowError()
    {
        $this->request->method('getResourceParam')
            ->will($this->returnCallback(array($this, 'mockOptionsEntityAccountID')));

        $this->request->method('getData')
            ->will($this->returnCallback(array($this, 'mockUpdateDataset')));


        $this->dbh->expects($this->once())->method('queryfirstcolumn')
            ->willReturn(0);

        $this->expectException('MiamiOH\RESTng\Exception\ResourceNotFound');
        $this->response = $this->mailableAccount->updateMailable();
        $this->assertEquals(\MiamiOH\RESTng\App::API_NOTFOUND, $this->response->getStatus());
    }

    public function testDeleteMailable()
    {
        $this->request->method('getResourceParam')
            ->will($this->returnCallback(array($this, 'mockOptionsEntityAccountID')));

        $this->dbh->expects($this->once())->method('queryfirstcolumn')
            ->willReturn(1);

        $this->response = $this->mailableAccount->deleteMailable();
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $this->response->getStatus());
//        $this->assertEquals($this->response->getPayload(), ['action'=> "mailable account deleted"]);
    }

    public function testFailedToDeleteNotExitsMailableId()
    {
        $this->request->method('getResourceParam')
            ->will($this->returnCallback(array($this, 'mockOptionsEntityAccountID')));

        $this->dbh->expects($this->once())->method('queryfirstcolumn')
            ->willReturn(0);

        $this->expectException('MiamiOH\RESTng\Exception\ResourceNotFound');
        $this->response = $this->mailableAccount->deleteMailable();

        $this->assertEquals(\MiamiOH\RESTng\App::API_NOTFOUND, $this->response->getStatus());
    }
}