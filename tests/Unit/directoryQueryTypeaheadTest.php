<?php


class DirectoryQueryTypeaheadTest extends \MiamiOH\RESTng\Testing\TestCase
{
    private $query;

    private $api;
    private $ldapFactoryObj;
    private $ldapObj;
    private $apiUser;

    private $ldapEntries = array();
    private $affiliation = array();

    protected function setup()
    {
        $this->ldapEntries = array();
        $this->affiliation = array();

        $this->api = $this->createMock(\MiamiOH\RESTng\App::class);

        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $this->apiUser = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthorized'))
            ->getMock();

        $this->apiUser->method('isAuthorized')->willReturn(1);

        $this->ldapFactoryObj = $this->getMockBuilder('\MiamiOH\RESTng\Connector\LDAPFactory')
            ->setMethods(array('getHandle'))
            ->getMock();

        $this->ldapObj = $this->getMockBuilder('\Dreamscapes\Ldap\Core\Ldap')
            ->setMethods(array('search'))
            ->getMock();

        $this->ldapResultObj = $this->getMockBuilder('\Dreamscapes\Ldap\Core\Result')
            ->disableOriginalConstructor()
            ->setMethods(array('getEntries', 'freeResult'))
            ->getMock();

        $this->query = new \MiamiOH\ProjectsDirectoryRest\Services\Query();

        $this->query->setApp($this->api);
        $this->query->setLdapFactory($this->ldapFactoryObj);
        $this->query->setApiUser($this->apiUser);

    }

    public function testQueryTypeahead()
    {

        $q = 'smith';

        $this->setLdapEntries();

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

        $request->expects($this->once())->method('getOptions')
            ->willReturn(array('q' => $q));

        $this->ldapFactoryObj->expects($this->once())->method('getHandle')->willReturn($this->ldapObj);
        $this->ldapObj->expects($this->once())->method('search')->willReturn($this->ldapResultObj);
        $this->ldapResultObj->method('getEntries')->willReturn($this->ldapEntries);

        $this->query->setRequest($request);

        $resp = $this->query->typeahead();

        $this->assertEquals(MiamiOH\RESTng\App::API_OK, $resp->getStatus());

        $payload = $resp->getPayload();

        $this->assertEquals(3, count($payload));

    }

    public function testQueryTypeaheadFilterAffiliation()
    {

        $q = 'smith';
        $this->affiliation = array('sta', 'fac');

        $this->setLdapEntries(['affiliation' => $this->affiliation]);

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

        $request->expects($this->once())->method('getOptions')
            ->willReturn(array('q' => $q, 'affiliation' => $this->affiliation));

        $this->ldapFactoryObj->expects($this->once())->method('getHandle')->willReturn($this->ldapObj);
        $this->ldapObj->expects($this->once())->method('search')
            ->with($this->anything(), $this->callback(array($this, 'searchWithFilter')), $this->anything())
            ->willReturn($this->ldapResultObj);
        $this->ldapResultObj->method('getEntries')->willReturn($this->ldapEntries);

        $this->query->setRequest($request);

        $resp = $this->query->typeahead();

        $this->assertEquals(MiamiOH\RESTng\App::API_OK, $resp->getStatus());

        $payload = $resp->getPayload();

        $this->assertEquals(2, count($payload));

        //print_r($payload);

    }

    private function setLdapEntries($options = array())
    {

        $entries = array(
            array(
                'uid' => 'smithb',
                'sn' => 'Smith',
                'givenname' => 'Bubba',
                'muohioeduprimaryaffiliationcode' => 'sta',
            ),
            array(
                'uid' => 'smithc',
                'sn' => 'Smith',
                'givenname' => 'Chuck',
                'muohioeduprimaryaffiliationcode' => 'und',
            ),
            array(
                'uid' => 'smithl',
                'sn' => 'Smith',
                'givenname' => 'Larry',
                'muohioeduprimaryaffiliationcode' => 'fac',
            ),
        );

        $this->ldapEntries = array(
            'count' => 0
        );

        $makeEntry = function($info) {
            $entry = array(
                'count' => 0
            );

            foreach ($info as $name => $value) {
                if (!is_array($value)) {
                    $value = array($value);
                }
                $entry[$name] = $value;
                $entry[$name]['count'] = count($value);

                $entry[$entry['count']] = $name;
                $entry['count']++;

                if ($name === 'uid') {
                    $entry['dn'] = 'uid=' . $value[0] . ',ou=people,dc=muohio,dc=edu';
                }
            }

            return $entry;
        };

        foreach ($entries as $entry) {
            if (isset($options['affiliation']) && !in_array($entry['muohioeduprimaryaffiliationcode'], $options['affiliation'])) {
                continue;
            }

            $this->ldapEntries[$this->ldapEntries['count']] = $makeEntry($entry);
            $this->ldapEntries['count']++;
        }

    }

    public function searchWithFilter($subject)
    {
        if ($this->affiliation) {
            foreach ($this->affiliation as $code) {
                $this->assertTrue(strpos($subject, 'muohioeduprimaryaffiliationcode=' . $code) !== false,
                    'search filter contains muohioeduprimaryaffiliationcode=' . $code);
            }
        }

        return true;
    }
}