<?php


class DirectoryQueryAuthorizationTest extends \MiamiOH\RESTng\Testing\TestCase
{
    private $query;

    private $api;
    private $ldapFactoryObj;
    private $ldapObj;
    private $apiUser;

    protected function setup()
    {

        $this->api = $this->createMock(\MiamiOH\RESTng\App::class);

        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $this->apiUser = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthenticated', 'isAuthorized'))
            ->getMock();

        $this->ldapFactoryObj = $this->getMockBuilder('\MiamiOH\RESTng\Connector\LDAPFactory')
            ->setMethods(array('getHandle'))
            ->getMock();

        $this->ldapObj = $this->getMockBuilder('\Dreamscapes\Ldap\Core\Ldap')
            ->setMethods(array('search'))
            ->getMock();

        $this->ldapResultObj = $this->getMockBuilder('\Dreamscapes\Ldap\Core\Result')
            ->disableOriginalConstructor()
            ->setMethods(array('getEntries', 'freeResult'))
            ->getMock();

        $this->query = new \MiamiOH\ProjectsDirectoryRest\Services\Query();

        $this->query->setApp($this->api);
        $this->query->setLdapFactory($this->ldapFactoryObj);
        $this->query->setApiUser($this->apiUser);

    }

    public function testQueryLookupAll()
    {

        $uid = 'testEntity';
        $firstName = 'Doe';
        $lastName = 'John';
        $affiliation = 'sta';

        $ldapResultData = array (
            'count' => 1,
            0 =>
                array (
                    'uid' =>
                        array (
                            'count' => 1,
                            0 => $uid,
                        ),
                    0 => 'uid',
                    'sn' =>
                        array (
                            'count' => 1,
                            0 => $lastName,
                        ),
                    1 => 'sn',
                    'givenname' =>
                        array (
                            'count' => 1,
                            0 => $firstName,
                        ),
                    2 => 'givenname',
                    'muohioeduprimaryaffiliationcode' =>
                        array (
                            'count' => 1,
                            0 => $affiliation,
                        ),
                    3 => 'muohioeduprimaryaffiliationcode',
                    'count' => 4,
                    'dn' => 'uid=' . $uid . ',ou=people,dc=muohio,dc=edu',
                ),
        );

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

        $request->expects($this->once())->method('getOptions')
            ->willReturn(array('q' => $uid));

        $this->ldapFactoryObj->expects($this->once())->method('getHandle')
            ->with($this->equalTo('DirectoryQueryAll'))
            ->willReturn($this->ldapObj);

        $this->ldapObj->expects($this->once())->method('search')->willReturn($this->ldapResultObj);
        $this->ldapResultObj->method('getEntries')->willReturn($ldapResultData);

        $this->apiUser->expects($this->once())->method('isAuthenticated')->willReturn(true);
        $this->apiUser->expects($this->once())->method('isAuthorized')->willReturn(1);

        $this->query->setRequest($request);

        $resp = $this->query->lookup();

        $this->assertEquals(MiamiOH\RESTng\App::API_OK, $resp->getStatus());

    }

    public function testQueryLookupUser()
    {

        $uid = 'testEntity';
        $firstName = 'Doe';
        $lastName = 'John';
        $affiliation = 'sta';

        $ldapResultData = array (
            'count' => 1,
            0 =>
                array (
                    'uid' =>
                        array (
                            'count' => 1,
                            0 => $uid,
                        ),
                    0 => 'uid',
                    'sn' =>
                        array (
                            'count' => 1,
                            0 => $lastName,
                        ),
                    1 => 'sn',
                    'givenname' =>
                        array (
                            'count' => 1,
                            0 => $firstName,
                        ),
                    2 => 'givenname',
                    'muohioeduprimaryaffiliationcode' =>
                        array (
                            'count' => 1,
                            0 => $affiliation,
                        ),
                    3 => 'muohioeduprimaryaffiliationcode',
                    'count' => 4,
                    'dn' => 'uid=' . $uid . ',ou=people,dc=muohio,dc=edu',
                ),
        );

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

        $request->expects($this->once())->method('getOptions')
            ->willReturn(array('q' => $uid));

        $this->ldapFactoryObj->expects($this->once())->method('getHandle')
            ->with($this->equalTo('DirectoryQueryUser'))
            ->willReturn($this->ldapObj);

        $this->ldapObj->expects($this->once())->method('search')->willReturn($this->ldapResultObj);
        $this->ldapResultObj->method('getEntries')->willReturn($ldapResultData);

        $this->apiUser->expects($this->once())->method('isAuthenticated')->willReturn(true);
        $this->apiUser->expects($this->once())->method('isAuthorized')->willReturn(0);

        $this->query->setRequest($request);

        $resp = $this->query->lookup();

        $this->assertEquals(MiamiOH\RESTng\App::API_OK, $resp->getStatus());

    }

    public function testQueryLookupPublic()
    {

        $uid = 'testEntity';
        $firstName = 'Doe';
        $lastName = 'John';
        $affiliation = 'sta';

        $ldapResultData = array (
            'count' => 1,
            0 =>
                array (
                    'uid' =>
                        array (
                            'count' => 1,
                            0 => $uid,
                        ),
                    0 => 'uid',
                    'sn' =>
                        array (
                            'count' => 1,
                            0 => $lastName,
                        ),
                    1 => 'sn',
                    'givenname' =>
                        array (
                            'count' => 1,
                            0 => $firstName,
                        ),
                    2 => 'givenname',
                    'muohioeduprimaryaffiliationcode' =>
                        array (
                            'count' => 1,
                            0 => $affiliation,
                        ),
                    3 => 'muohioeduprimaryaffiliationcode',
                    'count' => 4,
                    'dn' => 'uid=' . $uid . ',ou=people,dc=muohio,dc=edu',
                ),
        );

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

        $request->expects($this->once())->method('getOptions')
            ->willReturn(array('q' => $uid));

        $this->ldapFactoryObj->expects($this->once())->method('getHandle')
            ->with($this->equalTo('DirectoryQueryPublic'))
            ->willReturn($this->ldapObj);

        $this->ldapObj->expects($this->once())->method('search')->willReturn($this->ldapResultObj);
        $this->ldapResultObj->method('getEntries')->willReturn($ldapResultData);

        $this->apiUser->expects($this->once())->method('isAuthenticated')->willReturn(false);
        $this->apiUser->expects($this->never())->method('isAuthorized')->willReturn(0);

        $this->query->setRequest($request);

        $resp = $this->query->lookup();

        $this->assertEquals(MiamiOH\RESTng\App::API_OK, $resp->getStatus());

    }

}