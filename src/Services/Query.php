<?php

namespace MiamiOH\ProjectsDirectoryRest\Services;

class Query extends \MiamiOH\RESTng\Service
{

    private $ldapDataSources = [
        'public' => 'DirectoryQueryPublic',
        'user' => 'DirectoryQueryUser',
        'all' => 'DirectoryQueryAll',
    ];

    private $container = 'ou=people,dc=muohio,dc=edu';
    private $attributes = [
        'uid',
        'givenname',
        'sn',
        'muohioeduprimaryaffiliationcode',
        'muohioeduaffiliationcode',
        'displayname',
        'muohioeduhidden'
    ];

    /** @var  \MiamiOH\RESTng\Connector\LDAPFactory $ldapFactory */
    private $ldapFactory;

    public function setLdapFactory($ldapFactory)
    {
        $this->ldapFactory = $ldapFactory;
    }

    public function mailLookup()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $options = $request->getOptions();

        /** @var \Dreamscapes\Ldap\Core\Ldap $ldap */
        $ldap = $this->getLdapHandle();

        $ldapResult = $ldap->search($this->container, "mailAlternateAddress=" . $options['q'], $this->attributes);
        $results = $ldapResult->getEntries();

        if ($results['count'] === 1) {
            $entries = $this->cleanUpEntry($results);

            $payload = $this->makeQueryEntry($entries[0]);

            $response->setPayload($payload);
        } else {
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
        }

        return $response;
    }

    public function lookup()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $options = $request->getOptions();

        /** @var \Dreamscapes\Ldap\Core\Ldap $ldap */
        $ldap = $this->getLdapHandle();

        $ldapResult = $ldap->search($this->container, "uid=" . $options['q'], $this->attributes);
        $results = $ldapResult->getEntries();

        if ($results['count'] === 1) {
            $entries = $this->cleanUpEntry($results);

            $payload = $this->makeQueryEntry($entries[0]);

            $response->setPayload($payload);
        } else {
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
        }

        return $response;
    }

    public function typeahead()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $options = $request->getOptions();

        /** @var \Dreamscapes\Ldap\Core\Ldap $ldap */
        $ldap = $this->getLdapHandle();

        $filterList = ['(cn=*' . $options['q'] . '*)'];

        if (isset($options['affiliation'])) {
            $filterList[] = '(|' . implode('', array_map(function ($el) {
                    return '(muohioeduprimaryaffiliationcode=' . $el . ')';
                },
                    $options['affiliation'])) . ')';
        }

        $filter = '(&' . implode('', $filterList) . ')';

        $ldapResult = $ldap->search($this->container, $filter, $this->attributes);
        $results = $ldapResult->getEntries();

        $payload = [];

        foreach ($this->cleanUpEntry($results) as $entry) {
            $payload[] = $this->makeQueryEntry($entry);
        }

        $response->setPayload($payload);

        return $response;
    }

    private function getLdapHandle()
    {
        /** @var \MiamiOH\RESTng\Util\User $user */
        $user = $this->getApiUser();

        $queryLevel = 'public';

        if ($user->isAuthenticated()) {
            if ($user->isAuthorized('Directory Service', 'Query', 'All')) {
                $queryLevel = 'all';
            } else {
                $queryLevel = 'user';
            }
        }

        /** @var \Dreamscapes\Ldap\Core\Ldap $ldap */
        $ldap = $this->ldapFactory->getHandle($this->ldapDataSources[$queryLevel]);

        return $ldap;
    }

    private function makeQueryEntry($ldapEntry)
    {
        if (!isset($ldapEntry['uid'])) {
            throw new \Exception('Missing uid for ldap entry');
        }

        $qEntry = array();

        // TODO: account for missing indexes when constructing name
        $qEntry['uid'] = $ldapEntry['uid'];
        $qEntry['firstName'] = isset($ldapEntry['givenname']) ? $ldapEntry['givenname'] : '';
        $qEntry['lastName'] = isset($ldapEntry['sn']) ? $ldapEntry['sn'] : $ldapEntry['uid'];
        $qEntry['fullName'] = isset($ldapEntry['displayname']) ? $ldapEntry['displayname'] : $qEntry['lastName'];
        $qEntry['fullNameReverse'] = $qEntry['lastName'] . ($qEntry['firstName'] ? ', ' . $qEntry['firstName'] : '');

        $qEntry['hiddenStatus'] = isset($ldapEntry['muohioeduhidden']) ? $ldapEntry['muohioeduhidden'] : '';

        $qEntry['affiliation'] = isset($ldapEntry['muohioeduprimaryaffiliationcode'])
            ? $ldapEntry['muohioeduprimaryaffiliationcode'] : '';

        $qEntry['affiliationList'] = $this->getAffiliationList($ldapEntry);

        return $qEntry;
    }

    private function getAffiliationList(array $ldapEntry): array
    {
        if (empty($ldapEntry['muohioeduaffiliationcode'])) {
            return [];
        }

        if (is_array($ldapEntry['muohioeduaffiliationcode'])) {
            return $ldapEntry['muohioeduaffiliationcode'];
        }

        return array_filter([$ldapEntry['muohioeduaffiliationcode']]);
    }

    private function cleanUpEntry($entry)
    {
        $retEntry = array();
        for ($i = 0; $i < $entry['count']; $i++) {
            if (is_array($entry[$i])) {
                $subtree = $entry[$i];
                //This condition should be superfluous so just take the recursive call
                //adapted to your situation in order to increase perf.
//                if ( ! empty($subtree['dn']) and ! isset($retEntry[$subtree['dn']])) {
//                    $retEntry[$subtree['dn']] = $this->cleanUpEntry($subtree);
//                }
//                else {
                $retEntry[] = $this->cleanUpEntry($subtree);
//                }
            } else {
                $attribute = $entry[$i];
                if ($entry[$attribute]['count'] == 1) {
                    $retEntry[$attribute] = $entry[$attribute][0];
                } else {
                    for ($j = 0; $j < $entry[$attribute]['count']; $j++) {
                        $retEntry[$attribute][] = $entry[$attribute][$j];
                    }
                }
            }
        }
        return $retEntry;
    }

}
