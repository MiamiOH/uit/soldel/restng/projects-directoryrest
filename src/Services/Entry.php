<?php

namespace MiamiOH\ProjectsDirectoryRest\Services;

use MiamiOH\RESTng\Exception\ResourceNotFound;

class Entry extends \MiamiOH\RESTng\Service
{
    private $dbDataSource = 'MUWS_GEN_PROD';

    private $database, $dbh;

    public function setDatabase($database)
    {
        $this->database = $database;
        $this->dbh = $this->database->getHandle($this->dbDataSource);
    }

    /**
     * @return \MiamiOH\RESTng\Util\Response
     * @throws \MiamiOH\RESTng\Exception\BadRequest
     */
    public function getEntryAccountDetails()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $uniqueId = $request->getResourceParam('uniqueId');

        if (!isset($uniqueId)) {
            throw new \MiamiOH\RESTng\Exception\BadRequest('Missing uniqueId');
        }

        if (!$this->checkAccountExists($uniqueId)) {
            throw new ResourceNotFound();
        }

        $response->setPayload($this->collectAccountDetails($uniqueId));
        return $response;
    }

    /**
     * @param string $uniqueId
     * @return bool
     */
    private function checkAccountExists(string $uniqueId): bool
    {
        $result = $this->dbh->queryfirstcolumn('
         select count(*) from (
         select szbuniq_unique_id as uniqueId
            from szbuniq
            where szbuniq_unique_id = upper(?)
         union
         select gzbeatt_entity_id as uniqueId
            from gzbeatt
            where gzbeatt_entity_id = upper(?)
         union
         select username as uniqueId
            from safmgr.mm4f_family_member_info
            where lower(username) = lower(?)
         )
         ', $uniqueId, $uniqueId, $uniqueId);

        return $result ? true : false;
    }

    /**
     * @param string $uniqueId
     * @return array
     * @throws ResourceNotFound
     */
    private function collectAccountDetails(string $uniqueId): array
    {
        $details = $this->getAccountDetails($uniqueId);
        if (empty($details['type'])) {
            throw new ResourceNotFound("Server cannot find uniqueid");
        }
        return $details;
    }

    private function getAccountDetails(string $uniqueId): array
    {
        $result = $this->dbh->queryfirstcolumn('
         select count(*)
            from szbuniq
            where szbuniq_unique_id = upper(?)
         ', $uniqueId);

        if ($result) {
            return $this->getManagedAccountDetails($uniqueId);
        }
        return $this->getNonManagedAccountDetails($uniqueId);
    }

    private function getManagedAccountDetails(string $uniqueId): array
    {
//        $type = 'managed';
        $values = $this->dbh->queryfirstrow_assoc('
            select
                  lower(gzvagd1_unique_id) as uniqueid,
                  gzvagd1_first_name as firstName,
                  gzvagd1_last_name as lastName,
                  \'managed\' as type
               from gzvagd2
               where gzvagd1_unique_id = upper(?)
            ', $uniqueId);
        $values['id'] = '';
        return $this->formatData($values);
    }

    private function getFamilyAccountDetails(string $uniqueId): array
    {
        $result = $this->dbh->queryfirstcolumn('
            select count(*)
               from safmgr.mm4f_family_member_info
               where lower(username) = lower(?)
            ', $uniqueId);

        if ($result) {
            $values = $this->dbh->queryfirstrow_assoc('
               select
               family_member_id as id,
                     lower(USERNAME) as uniqueid,
                     first_name as firstName,
                     last_name as lastName,
                     \'family\' as type
                  from safmgr.mm4f_family_member_info
                  where lower(safmgr.mm4f_family_member_info.username) = lower(?)
               ', $uniqueId);
            return $this->formatData($values);
        }
    }

    private function formatData(array $data): array
    {
        $formattedData = $this->initAccountDetails();
        $formattedData['id'] = $data['id'];
        $formattedData['uniqueId'] = $data['uniqueid'];
        $formattedData['type'] = $data['type'];
        $formattedData['firstName'] = $data['firstname'];
        $formattedData['lastName'] = $data['lastname'];
        return $formattedData;
    }

    private function getEntityAccountDetails(string $uniqueId): array
    {
        $entityValues = $this->dbh->queryfirstrow_assoc('
               select
                     lower(gzbeatt_entity_id) as uniqueId,
                     gzbeatt_status as entity_type
                  from gzbeatt
                  where gzbeatt_entity_id = upper(?)
               ', $uniqueId);

        $ldap = $this->callResource(
            'directory.v1.query.lookup', ['options' => ['q' => $uniqueId]]);
        $values = $ldap->getPayload();
        return $this->formatLdapValues($values, $entityValues);
    }

    private function formatLdapValues(array $ldapValues, array $entityValues)
    {
        $formattedLdap = $this->initAccountDetails();
        $formattedLdap['uniqueId'] = $entityValues['uniqueid'];
        $formattedLdap['type'] = $this->getEntityAccountType($entityValues['entity_type']);

        if (!empty($ldapValues['firstName'])) {
            $formattedLdap['firstName'] = $ldapValues['firstName'];
        }

        if (!empty($ldapValues['lastName'])) {
            $formattedLdap['lastName'] = $ldapValues['lastName'];
        }
        return $formattedLdap;
    }

    public function initAccountDetails()
    {
        return [
            'id' => '',
            'uniqueId' => '',
            'type' => '',
            'firstName' => '',
            'lastName' => '',
        ];
    }

    private function getEntityAccountType(string $typeCode): string
    {
        switch ($typeCode) {
            case 'a':    // Active entity
            case 'i':    // Inactive entity
                $type = 'entity';
                break;

            case 'm':    // Email alias
                $type = 'alias';
                break;

            case 'f':    // Family member
                $type = 'family';
                break;

            default:
                $type = 'other';
        }
        return $type;
    }

    private function getNonManagedAccountDetails(string $uniqueId): array
    {
        $result = $this->dbh->queryfirstcolumn('
            select count(*)
               from gzbeatt
               where gzbeatt_entity_Id = upper(?)
            ', $uniqueId);

        if ($result) {
            return $this->getEntityAccountDetails($uniqueId);
        }
        return $this->getFamilyAccountDetails($uniqueId);
    }

    /**
     * @return \MiamiOH\RESTng\Util\Response
     * @throws \Exception
     */
    public function createEntry()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $data = $request->getData();

        if (empty($data)) {
            $payload['errors'][] = 'No Data';
            $response->setPayload($payload);
            $response->setStatus(\MiamiOH\RESTng\App::API_BADREQUEST);
            return $response;
        }

        $this->validateData($data);

        if ($data['id'] && $data['type']) {
            switch ($data['type']) {
                case 'family':
                    $this->validateAccountDoesNotExists($data['id']);
                    $this->validateFamilyMemberDoesExists($data['id']);

                    try {
                        $this->createFamilyAccount($data['id']);
                    } catch (\Exception $e) {
                        $response->setPayload([$e->getMessage()]);
                        $response->setStatus(\MiamiOH\RESTng\App::API_FAILED);
                        return $response;
                    }
                    $response->setPayload([$data]);
                    $response->setStatus(\MiamiOH\RESTng\App::API_CREATED);
                    return $response;
                    break;
                default:
                    throw new \MiamiOH\RESTng\Exception\BadRequest("invalid or unsupported type");
            }

        } else {
            throw new \MiamiOH\RESTng\Exception\BadRequest("missing id or type");
        }
    }

    public function updateEntry()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $status = \MiamiOH\RESTng\App::API_OK;

        $paramUniqueId = $request->getResourceParam('uniqueId');

        $data = $request->getData();

        if (empty($data)) {
            throw new \MiamiOH\RESTng\Exception\BadRequest('Invalid request');
        }

        $this->checkUniqueId($data['uniqueId'], $paramUniqueId);
        $pidm = $this->getPidm($data['uniqueId']);

        $serviceInfo = [];

        if (empty($data['services'])) {
            try {
                $result = $this->dbh->perform('update gzragst set gzragst_status_ind = ?, gzragst_progress_ind = ?
			    where gzragst_pidm = ?
				', 'M', 'P', $pidm);
            }catch (\Exception $e) {
                $response->setPayload([$e->getMessage()]);
                $response->setStatus(\MiamiOH\RESTng\App::API_FAILED);
                return $response;
            }
        }else {
            foreach ($data['services'] as $service) {
                if (empty($service['status'])) {
                    $service['status'] = 'M';
                }
                if ((!empty($service['status'])) && ($service['status']) !== 'M') {
                    throw new \MiamiOH\RESTng\Exception\BadRequest("invalid status");
                }
                $serviceNames = [
                    'name' => $service['name'],
                    'status' => $service['status']
                ];

                array_push($serviceInfo, $serviceNames);
            }

            foreach($serviceInfo as $info){
                try {
                    $result = $this->dbh->perform('update gzragst set gzragst_status_ind = ?, gzragst_progress_ind = ?
			        where gzragst_pidm = ? and gzragst_server_name = ?
				    ', $info['status'], 'P', $pidm, $info['name']);
                }catch (\Exception $e) {
                    $response->setPayload([$e->getMessage()]);
                    $response->setStatus(\MiamiOH\RESTng\App::API_FAILED);
                    return $response;
                }
            }
        }

        $response->setStatus($status);
        return $response;

    }

    private function getPidm(string $uniqueid)
    {
        $pidm = $this->dbh->queryfirstcolumn('
			select szbuniq_pidm
				from szbuniq
				where szbuniq_unique_id = upper(?)
			', $uniqueid);

        if(empty($pidm)){
            throw new \MiamiOH\RESTng\Exception\BadRequest("unable to find PIDM for uniqueid");
        }
        return $pidm;
    }

    private function createFamilyAccount(string $id)
    {
        return $this->dbh->perform('
         insert into gzbfmacc (gzbfmacc_member_id, gzbfmacc_status_ind, gzbfmacc_progress_ind, gzbfmacc_activity_date)
            values (?, ?, ?, sysdate)
         ', $id, 'N', 'P');

    }

    private function validateAccountDoesNotExists(int $id): void
    {
        $result = $this->dbh->queryfirstcolumn('
         select count(*)
            from gzbfmacc
            where gzbfmacc_member_id = ?
         ', $id);

        if((int)$result === 1){
            throw new \MiamiOH\RESTng\Exception\BadRequest("account already exist");
        }
    }

    private function validateFamilyMemberDoesExists(int $id): void
    {
        $result = $this->dbh->queryfirstcolumn('
         select count(*)
            from safmgr.mm4f_family_member_info
            where family_member_id = ?
         ', $id);

        if((int)$result === 0){
            throw new \MiamiOH\RESTng\Exception\BadRequest("cannot find family member record");
        }
    }

    private function validateData(array $data)
    {
        $this->validateId($data['id']);
        $this->validateUniqueId($data['uniqueId']);
        $this->validateType($data['type']);
        $this->validateFirstName($data['firstName']);
        $this->validateLastName($data['lastName']);
    }
    private function validateId(string $id)
    {
        //This is applicable only for 'family' type
        if (empty($id) || !(is_numeric($id))) {
            throw new \MiamiOH\RESTng\Exception\BadRequest('Entry id cannot be empty or non-numeric');
        }
    }

    private function validateUniqueId(string $uniqueId)
    {
        if (empty($uniqueId)) {
            throw new \MiamiOH\RESTng\Exception\BadRequest('unique id cannot be empty');
        }
        //This is applicable only for 'family' type
        if (filter_var($uniqueId, FILTER_VALIDATE_EMAIL) === false) {
            throw new \MiamiOH\RESTng\Exception\BadRequest("not a valid email address");
        }
    }

    private function validateType(string $type)
    {
        if (empty($type)) {
            throw new \MiamiOH\RESTng\Exception\BadRequest('type cannot be empty');
        }
    }

    private function validateFirstName(string $firstName)
    {
        if (empty($firstName)) {
            throw new \MiamiOH\RESTng\Exception\BadRequest('first name cannot be empty');
        }
    }

    private function validateLastName(string $lastName)
    {
        if (empty($lastName)) {
            throw new \MiamiOH\RESTng\Exception\BadRequest('last name cannot be empty');
        }
    }

    private function checkUniqueId(string $uniqueId, string $paramUniqueId)
    {
        if (empty($uniqueId) || $uniqueId !== $paramUniqueId) {
            throw new \MiamiOH\RESTng\Exception\BadRequest('uniqueId empty or is not matching with body');
        }
    }
}