<?php

namespace MiamiOH\ProjectsDirectoryRest\Services;

class Alias extends \MiamiOH\RESTng\Service
{

    private $ldapDataSource = 'DirectoryQueryAll';
    private $dbDataSource = 'directory_alias_recorder';

    private $container = 'ou=people,dc=muohio,dc=edu';

    /** @var  \MiamiOH\RESTng\Util\Configuration $configuration */
    private $configuration;

    /** @var  \MiamiOH\RESTng\Connector\LDAPFactory $ldapFactory */
    private $ldapFactory;
    private $database;

    public function setLdapFactory($ldapFactory)
    {
        $this->ldapFactory = $ldapFactory;
    }

    public function setDatabase($database)
    {
        $this->database = $database;
    }

    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
    }

    public function checkAliases()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $options = $request->getOptions();

        if (!isset($options['alias'])) {
            throw new \MiamiOH\RESTng\Exception\BadRequest('Missing alias argument');
        }

        $aliasCollection = [];

        foreach ($options['alias'] as $alias) {
            $aliasCollection[] = $this->checkAlias($alias);
        }

        $response->setPayload($aliasCollection);

        return $response;
    }

    public function createAliases()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $uniqueId = $request->getResourceParam('uniqueId');
        $data = $request->getData();

        if (!isset($data['aliases']) || !is_array($data['aliases'])) {
            throw new \MiamiOH\RESTng\Exception\BadRequest('Array of aliases is required');
        }

        $dbh = $this->database->getHandle($this->dbDataSource);

        $results = [
            'name' => $data['name'],
            'phone' => $data['phone'],
            'aliases' => []
        ];

        $aliases = array_reduce(
            $data['aliases'],
            function ($c, $info) {
                $c[strtolower($info['alias'])] = $info;
                return $c;
            },
            []
        );

        if (!empty($aliases)) {
            $placeHolders = implode(', ', array_fill(0, count($aliases), '?'));

            $dbh->perform('
                delete from gzbeatt
                    where gzbeatt_owner_id = upper(?)
                        and gzbeatt_status = ?
                        and lower(gzbeatt_long_name) not in (' . $placeHolders . ')'
                , array_merge([$uniqueId, 'm'], array_keys($aliases)));
        }

        foreach ($aliases as $alias => $info) {
            $checked = $this->checkAlias($alias);
            $update = $info;
            $update['action'] = 'none';

            if ($checked['is_available'] ||
                (
                    !$checked['is_available']
                    && !($checked['is_alias'] || $checked['is_uid'] || $checked['is_entity'])
                    && $checked['owner'] === $uniqueId
                    && $checked['is_mailable']
                )
            ) {
                list($username, $domain) = explode('@', $alias);

                $dbh->perform('
                    insert into gzbeatt (gzbeatt_entity_id, gzbeatt_status, gzbeatt_owner_id, gzbeatt_long_name,
                            gzbeatt_ct_number, gzbeatt_create_date)
                        values (upper(?), ?, upper(?), ?, null, current_date)
                ', $username, 'm', $uniqueId, $alias);

                $update['action'] = 'created';
            }

            $results['aliases'][] = $update;
        }
        $config = $this->configuration->getConfiguration('WebDirectory', 'ApplicationValue');

        $replacements = [
            'uniqueId' => $uniqueId,
            'name' => $data['name'],
            'phone' => $data['phone'],
            'mail' => $data['mail'],
            'aliases' => implode("\n", array_reduce(
                $data['aliases'],
                function ($c, $alias) {
                    $c[] = $alias['alias'];
                    return $c;
                },
                []
            )),
        ];

        $replace = function ($string) use ($replacements) {
            foreach ($replacements as $key => $value) {
                $string = str_replace('{{' . $key . '}}', $value, $string);
            }
            return $string;
        };

        $notification = array('priority' => 3);
        $notification['toAddr'] = $replace($config['alias.review.toaddress']);
        $notification['fromAddr'] = $replace($config['alias.review.fromaddress']);
        $notification['subject'] = $replace($config['alias.review.subject']);
        $notification['body'] = $replace($config['alias.review.body']);

        $notificationPayload = array();
        $notificationPayload['dataType'] = 'model';
        $notificationPayload['data'] = $notification;

        $notificationResponse = $this->callResource('notification.v1.email.message.create', array(
            'data' => $notificationPayload
        ));

        // Check that the call was successful.
        if ($notificationResponse->getStatus() !== \MiamiOH\RESTng\App::API_CREATED) {
            throw new \Exception('Notification request failed: ' . print_r($notificationResponse->getStatus(), true));
        }


        $response->setStatus(\MiamiOH\RESTng\App::API_CREATED);
        $response->setPayload($results);

        return $response;
    }

    private function checkAlias($alias)
    {
        $dbh = $this->database->getHandle($this->dbDataSource);

        $ldap = $this->ldapFactory->getHandle($this->ldapDataSource);

        list($username, $domain) = explode('@', $alias);
        $data = [
            'alias' => strtolower($alias),
            'owner' => '',
            'is_available' => false,
        ];

        $isUid = $dbh->queryfirstcolumn('
                select count(*)
                    from szbuniq
                    where szbuniq_unique_id = upper(?)
                ', $username);

        $data['is_uid'] = (int)$isUid === 1;

        $isEntity = $dbh->queryfirstcolumn('
                select count(*)
                    from gzbeatt
                    where gzbeatt_entity_id = upper(?)
                        and gzbeatt_status != ?
                ', $username, 'm');

        $data['is_entity'] = (int)$isEntity === 1;

        $isAlias = $dbh->queryfirstcolumn('
                select count(*)
                    from gzbeatt
                    where lower(gzbeatt_long_name) = lower(?)
                        and gzbeatt_status = ?
                ', $alias, 'm');

        $data['is_alias'] = (int)$isAlias === 1;

        if ($data['is_alias']) {
            $data['owner'] = $dbh->queryfirstcolumn('
                    select lower(gzbeatt_owner_id)
                        from gzbeatt
                        where lower(gzbeatt_long_name) = lower(?)
                            and gzbeatt_status = ?
                    ', $alias, 'm');
        }

        $ldapResult = $ldap->search(
            $this->container,
            "(|(mail=$alias)(mailAlternateAddress=$alias))",
            ['uid']
        );

        $results = $ldapResult->getEntries();

        $data['is_mailable'] = $results['count'] >= 1;

        if ($data['is_mailable'] && $results['count'] === 1) {
            $data['owner'] = isset($results[0]['uid'][0]) ? $results[0]['uid'][0] : '';
        }

        $data['is_available'] = !($data['is_uid'] || $data['is_entity'] || $data['is_alias'] || $data['is_mailable']);

        return $data;
    }
}
