<?php

namespace MiamiOH\ProjectsDirectoryRest\Services;

use MiamiOH\RESTng\Exception\ResourceNotFound;

class Mailable extends \MiamiOH\RESTng\Service
{
    private $dbDataSource = 'MUWS_GEN_PROD';

    private $database;

    public function setDatabase($database)
    {
        $this->database = $database;
    }

    /**
     * @return \MiamiOH\RESTng\Util\Response
     * @throws \Exception
     */
    public function createMailable()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $status = \MiamiOH\RESTng\App::API_CREATED;
        $data = $request->getData();

        if (empty($data)) {
            $payload['errors'][] = 'No Data';
            $status = \MiamiOH\RESTng\App::API_BADREQUEST;
            $response->setPayload($payload);
            $response->setStatus($status);
            return $response;
        }

        $this->validateData($data);

        if($this->isExists($data['id'])){
            throw new \MiamiOH\RESTng\Exception\BadRequest('Account Mailable Already exists.');
        }

        $dbh = $this->database->getHandle($this->dbDataSource);

        try {
            $dbh->perform('
                    insert into gzbeatt (gzbeatt_entity_id, gzbeatt_status, gzbeatt_owner_id, gzbeatt_long_name,
                            gzbeatt_ct_number, gzbeatt_create_date)
                        values (upper(?), ?, upper(?), ?, null, current_date)
                ', $data['id'], $data['status'], $data['owner'], $data['longName']);

        } catch (\Exception $e) {
            $response->setPayload([$e->getMessage()]);
            $response->setStatus(\MiamiOH\RESTng\App::API_FAILED);
            return $response;
        }

//        $response->setPayload(['action' => 'mailable account created']);
        $response->setPayload([$data]);
        $response->setStatus($status);
        return $response;

    }

    /**
     * @return \MiamiOH\RESTng\Util\Response
     * @throws \MiamiOH\RESTng\Exception\BadRequest
     */
    public function getMailable()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $id = $request->getResourceParam('id');

        if (!isset($id)) {
            throw new \MiamiOH\RESTng\Exception\BadRequest('Missing mailable account ID argument');
        }

        if(!$this->isExists($id)){
            throw new ResourceNotFound();
        }
        $response->setPayload($this->checkMailableDetails($id));
        return $response;

    }

    /**
     * @param string $accountId
     * @return array
     */
    private function checkMailableDetails(string $accountId): array
    {
        $dbh = $this->database->getHandle($this->dbDataSource);

        $results = $dbh->queryfirstrow_assoc(
            'select *
                        from gzbeatt
                        where gzbeatt_entity_id = upper(?)', $accountId);

        $data = [];

        $data['id'] = $results['gzbeatt_entity_id'];
        $data['longName'] = $results['gzbeatt_long_name'];
        $data['owner'] = $results['gzbeatt_owner_id'];
        $data['status'] = $results['gzbeatt_status'];

        return $data;
    }

    private function isExists(string $accountId):bool
    {
        $dbh = $this->database->getHandle($this->dbDataSource);
        $exists = $dbh->queryfirstcolumn('
                select count(*)
                        from gzbeatt
                        where gzbeatt_entity_id = upper(?)', $accountId);

        /*print_r($exists);
        exit;*/
        return $exists ? true : false;

    }

    /**
     * @return \MiamiOH\RESTng\Util\Response
     * @throws \MiamiOH\RESTng\Exception\BadRequest
     */
    public function updateMailable()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $status = \MiamiOH\RESTng\App::API_OK;
        $id = $request->getResourceParam('id');

        if (!isset($id)) {
            throw new \MiamiOH\RESTng\Exception\BadRequest('Missing mailable id argument');
        }

        $data = $request->getData();

        if (empty($data)) {
            $payload['errors'][] = 'No Data';
            $status = \MiamiOH\RESTng\App::API_BADREQUEST;

            $response->setPayload($payload);
            $response->setStatus($status);
            return $response;
        }

        if (isset($data['owner'])) {
            $this->validateOwner($data['owner']);
        }
        if (isset($data['status'])) {
            $this->validateStatus($data['status']);
        }
        if (isset($data['longName'])) {
            $this->validateLongName($data['longName']);
        }

        if(!$this->isExists($id)){
            throw new ResourceNotFound();
        }

        $sql = $this->constructUpdateSql($data, $id);

        $dbh = $this->database->getHandle($this->dbDataSource);
        try {
            $dbh->perform($sql['query'], $sql['values']);
        } catch (\Exception $e) {
            $response->setPayload([$e->getMessage()]);
            $response->setStatus(\MiamiOH\RESTng\App::API_FAILED);
            return $response;
        }
        $response->setStatus($status);
        return $response;

    }

    private function constructUpdateSql(array $data, string $id): array
    {
        $columns = [];
        $values = [];

        if (!empty($data['owner'])) {
            $columns[] = 'gzbeatt_owner_id = ? ';
            $values[] = $data['owner'];
        }

        if (!empty($data['status'])) {
            $columns[] = 'gzbeatt_status =  ? ';
            $values[] = $data['status'];
        }

        if (!empty($data['longName'])) {
            $columns[] = 'gzbeatt_long_name =  ? ';
            $values[] = $data['longName'];
        }

        $values[] = $id;

        $query = "UPDATE GZBEATT SET " . implode(', ',
                $columns) . " WHERE gzbeatt_entity_id= upper(?)";

        return ['query' => $query, 'values' => $values];
    }

    /**
     * @return \MiamiOH\RESTng\Util\Response
     * @throws \MiamiOH\RESTng\Exception\BadRequest
     */
    public function deleteMailable()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $dbh = $this->database->getHandle($this->dbDataSource);

        $status = \MiamiOH\RESTng\App::API_OK;
        $id = $request->getResourceParam('id');
        if (!isset($id)) {
            throw new \MiamiOH\RESTng\Exception\BadRequest('Missing mailable account ID argument');
        }

        if(!$this->isExists($id)){
            throw new ResourceNotFound();
        }

        try {
            $dbh->perform('
                    delete from gzbeatt where 
                    gzbeatt_entity_id= upper(?)',
                $id);

        } catch (\Exception $e) {
            $response->setPayload([$e->getMessage()]);
            $response->setStatus(\MiamiOH\RESTng\App::API_FAILED);
            return $response;
        }

//        $response->setPayload(['action' => 'mailable account deleted']);
        $response->setStatus($status);
        return $response;

    }

    /**
     * @param array $data
     * @throws \MiamiOH\RESTng\Exception\BadRequest
     */
    private function validateData(array $data)
    {
        $this->validateId($data['id']);
        $this->validateLongName($data['longName']);
        $this->validateOwner($data['owner']);
        $this->validateStatus($data['status']);
    }


    /**
     * @param string $ownerUniqueId
     * @throws \MiamiOH\RESTng\Exception\BadRequest
     */
    private function validateOwner(string $ownerUniqueId)
    {
        if (empty($ownerUniqueId)) {
            throw new \MiamiOH\RESTng\Exception\BadRequest('Owner cannot be empty');
        }
        if (!preg_match('/^\w{1,8}$/', $ownerUniqueId)) {
            throw new \MiamiOH\RESTng\Exception\BadRequest('Owner value must be a valid uniqueId');
        }
    }

    private function validateStatus(string $status)
    {
        if (empty($status)) {
            throw new \MiamiOH\RESTng\Exception\BadRequest('status cannot be empty');
        }
        if (strlen($status) > 1) {
            throw new \MiamiOH\RESTng\Exception\BadRequest('status length cannot be more than 1');
        }
    }

    private function validateLongName(string $longName)
    {
        if (empty($longName)) {
            throw new \MiamiOH\RESTng\Exception\BadRequest('Mailable long name cannot be empty');
        }
    }
    private function validateId(string $id)
    {
        if (empty($id)) {
            throw new \MiamiOH\RESTng\Exception\BadRequest('Mailable id cannot be empty');
        }
    }
}
