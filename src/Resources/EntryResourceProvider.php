<?php

namespace MiamiOH\ProjectsDirectoryRest\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class EntryResourceProvider extends ResourceProvider
{
    public function registerDefinitions(): void
    {
        $this->addDefinition([
            'name' => 'Directory.Entry.Model',
            'type' => 'object',
            'properties' => [
                'id' => [
                    'type' => 'integer',
                    'description' => 'id of the entry account'
                ],
                'uniqueId' => [
                    'type' => 'string',
                    'description' => 'uniqueId of the entry account.'
                ],
                'type' =>
                    [
                        'type' => 'string',
                        'description' => 'Type of the entry account'
                    ],
                'firstName' =>
                    [
                        'type' => 'string',
                        'description' => 'First Name of the entry account'
                    ],
                'lastName' =>
                    [
                        'type' => 'string',
                        'description' => 'Last Name of the entry account'
                    ],
            ]
        ]);
        $this->addDefinition([
            'name' => 'Directory.Entry.Post.Model',
            'type' => 'object',
            'properties' => [
                'id' => [
                    'type' => 'integer',
                    'enum' => ['required|integer'],
                    'description' => 'numeric Id is required for POST'
                ],
                'uniqueId' => [
                    'type' => 'string',
                    'enum' => ['required|string'],
                    'description' => 'Email address is required for POST'
                ],
                'type' => [
                    'type' => 'string',
                    'enum' => ['required|string'],
                    'description' => 'Type of the entry'
                ],
                'firstName' =>
                    [
                        'type' => 'string',
                        'enum' => ['required|string'],
                        'description' => 'First Name of the entry'
                    ],
                'lastName' => [
                    'type' => 'string',
                    'enum' => ['required|string'],
                    'description' => 'Last Name of the entry'
                ],
            ]
        ]);

        $this->addDefinition([
            'name' => 'Directory.Entry.Status',
            'type' => 'object',
            'properties' => [
                'uniqueId' => [
                    'type' => 'string',
                    'enum' => ['required|string'],
                    'description' => 'UniqueId of the Entry'
                ],
                'services' => [
                    'type' => 'array',
                    'description' => 'List of Services',
                    '$ref' => '#/definitions/Directory.Entry.Status.Collection'
                ],
            ]
        ]);

        $this->addDefinition(array(
            'name' => 'Directory.Entry.Status.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Directory.Entry.Status.Service'
            )
        ));

        $this->addDefinition([
            'name' => 'Directory.Entry.Status.Service',
            'type' => 'object',
            'properties' => [
                'name' => [
                    'type' => 'string',
                    'description' => 'Name of the service'
                ],
                'status' => [
                    'type' => 'string',
                    'description' => 'Status of the entry'
                ],
            ]
        ]);
    }

    public function registerServices(): void
    {
        $this->addService([
            'name' => 'Directory\Entry',
            'class' => 'MiamiOH\ProjectsDirectoryRest\Services\Entry',
            'description' => 'Provides Entry account information',
            'set' => [
                'database' => ['type' => 'service', 'name' => 'APIDatabaseFactory'],
            ],
        ]);
    }

    public function registerResources(): void
    {
        $this->addResource([
            'action' => 'read',
            'name' => 'directory.v1.entry.uniqueId',
            'description' => 'Get Entry Account details',
            'pattern' => '/directory/v1/entry/:uniqueId',
            'service' => 'Directory\Entry',
            'params' => [
                'uniqueId' => ['description' => 'Enter Account uniqueId.'],
            ],
            'method' => 'getentryAccountDetails',
            'returnType' => 'model',
            'tags' => ['Directory'],
            'middleware' => [
                'authorize' => [
                    'application' => 'Directory Service',
                    'module' => 'Entry',
                    'key' => 'read'
                ],
                'authenticate' => [
                    [
                        'type' => 'token'
                    ],
                ],
            ],
            'responses' => [
                App::API_OK => [
                    'description' => 'Entry Account Information',
                    'returns' => [
                        'type' => 'model',
                        'schema' => [
                            '$ref' => '#/definitions/Directory.Entry.Model',
                        ]
                    ]
                ],
                App::API_NOTFOUND => [
                    'description' => 'Entry Account not found',
                ]
            ]
        ]);

        $this->addResource([
            'action' => 'create',
            'name' => 'directory.v1.Entry.create.uniqueId',
            'description' => 'Create Family Member Entry Account',
            'pattern' => '/directory/v1/entry',
            'service' => 'Directory\Entry',
            'method' => 'createEntry',
            'tags' => ['Directory'],
            'middleware' => [
                'authorize' => [
                    'application' => 'Directory Service',
                    'module' => 'Entry',
                    'key' => 'create'
                ],
                'authenticate' => [
                    [
                        'type' => 'token'
                    ],
                ],
            ],
            'body' => [
                'description' => 'Entry Account information',
                'required' => true,
                'schema' => [
                    '$ref' => '#/definitions/Directory.Entry.Post.Model'
                ]
            ],
            'responses' => [
                App::API_CREATED => [
                    'description' => 'Entry Account created successfully.',
                ],
                App::API_BADREQUEST => [
                    'description' => 'Some or all data are bad.',
                ],
                App::API_FAILED => [
                    'description' => 'Create operation failed.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],
            ],
        ]);

        $this->addResource([
            'action' => 'update',
            'name' => 'directory.v1.entryStatus.put.uniqueId',
            'description' => 'Update Entry Account',
            'pattern' => '/directory/v1/entryStatus/:uniqueId',
            'service' => 'Directory\Entry',
            'params' => [
                'uniqueId' => ['description' => 'Enter Account uniqueId.']
            ],
            'method' => 'updateEntry',
            'tags' => ['Directory'],
            'middleware' => [
                'authorize' => [
                    'application' => 'Directory Service',
                    'module' => 'Entry',
                    'key' => 'update'
                ],
                'authenticate' => [
                    [
                        'type' => 'token'
                    ],
                ],
            ],
            'body' => [
                'description' => 'Entity Account information',
                'required' => true,
                'schema' => [
                    '$ref' => '#/definitions/Directory.Entry.Status'
                ]
            ],
            'responses' => [
                App::API_OK => [
                    'description' => 'Updated successfully.',
                ],
                App::API_BADREQUEST => [
                    'description' => 'Some or all data are bad.',
                ],
                App::API_FAILED => [
                    'description' => 'Update operation failed.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],
            ],
        ]);

    }

    public function registerOrmConnections(): void
    {

    }
}