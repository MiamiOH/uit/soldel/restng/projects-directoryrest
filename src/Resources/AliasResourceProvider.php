<?php

namespace MiamiOH\ProjectsDirectoryRest\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class AliasResourceProvider extends ResourceProvider
{


    public function registerDefinitions(): void
    {

        $this->addDefinition(array(
            'name' => 'Directory.Alias.Entry',
            'type' => 'object',
            'properties' => array(
                'alias' => array('type' => 'string', 'description' => 'alias value'),
                'owner' => array('type' => 'string', 'description' => 'owner if alias'),
                'is_available' => array('type' => 'boolean', 'description' => 'indicates if alias is available'),
                'is_uid' => array('type' => 'boolean', 'description' => 'indicates if alias is a uniqueid'),
                'is_entity' => array('type' => 'boolean', 'description' => 'indicates if alias is an entity'),
                'is_alias' => array('type' => 'boolean', 'description' => 'indicates if alias exists for another entry'),
                'is_mailable' => array('type' => 'boolean', 'description' => 'indicates is alias is mailable'),
            )
        ));

        $this->addDefinition(array(
            'name' => 'Directory.Alias.Entry.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Directory.Alias.Entry'
            )
        ));

        $this->addDefinition(array(
            'name' => 'Directory.Alias.Create.Detail',
            'type' => 'object',
            'properties' => array(
                'alias' => array(
                    'type' => 'string',
                ),
                'primary' => array(
                    'type' => 'boolean',
                )
            )
        ));

        $this->addDefinition(array(
            'name' => 'Directory.Alias.Create',
            'type' => 'object',
            'properties' => array(
                'name' => array('type' => 'string', 'description' => 'owner name'),
                'phone' => array('type' => 'string', 'description' => 'owner phone'),
                'mail' => array('type' => 'string', 'description' => 'owner email address'),
                'aliases' => array(
                    'type' => 'array',
                    'items' => [
                        'type' => 'object',
                        '$ref' => '#/definitions/Directory.Alias.Create.Detail'
                    ]
                ),
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'Directory\Alias',
            'class' => 'MiamiOH\ProjectsDirectoryRest\Services\Alias',
            'description' => 'Provides directory alias management functions',
            'set' => array(
                'ldapFactory' => array('type' => 'service', 'name' => 'APILDAPFactory'),
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
                'configuration' => array('type' => 'service', 'name' => 'APIConfiguration'),
            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'directory.v1.alias.get.collection',
            'description' => 'Get information about aliases',
            'pattern' => '/directory/v1/alias',
            'service' => 'Directory\Alias',
            'method' => 'checkAliases',
            'options' => array(
                'alias' => array('type' => 'list', 'description' => 'comma separated list of aliases'),
            ),
            'returnType' => 'collection',
            'tags' => array('Directory'),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    'application' => 'WebServices',
                    'module' => 'EmailAlias',
                    'key' => 'view'),

            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'Alias entry collection',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/Directory.Alias.Entry.Collection',
                    )
                )
            )
        ));

        $this->addResource(array(
            'action' => 'create',
            'name' => 'directory.v1.alias.create.uniqueid',
            'description' => 'Create new aliases for a user',
            'pattern' => '/directory/v1/alias/:uniqueId',
            'service' => 'Directory\Alias',
            'method' => 'createAliases',
            'tags' => array('Directory'),
            'params' => array(
                'uniqueId' => array('description' => 'UniqueId owning the aliases'),
            ),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    'application' => 'WebServices',
                    'module' => 'EmailAlias',
                    'key' => 'update'),

            ),
            'body' => array(
                'description' => 'User contact information and list of aliases',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/Directory.Alias.Create'
                )
            ),
            'responses' => array(
                App::API_CREATED => array(
                    'description' => 'A successful response',
                    '$ref' => '#/definitions/Directory.Alias.Create'
                )
            )
        ));

    }

    public function registerOrmConnections(): void
    {

    }
}