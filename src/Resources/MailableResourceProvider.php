<?php

namespace MiamiOH\ProjectsDirectoryRest\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class MailableResourceProvider extends ResourceProvider
{
    public function registerDefinitions(): void
    {
        $this->addDefinition([
            'name' => 'Directory.Mailable.Model',
            'type' => 'object',
            'properties' => [
                'id' => [
                    'type' => 'string',
                    'description' => 'Mailable id is required for POST or PUT or DELETE.'
                ],
                'longName' =>
                    [
                        'type' => 'string',
                        'description' => 'LongName of the Mailable Entity Account'
                    ],
                'owner' =>
                    [
                        'type' => 'string',
                        'description' => 'Owner of the Mailable Entity Account'
                    ],
                'status' =>
                    [
                        'type' => 'string',
                        'description' => 'Status of the Mailable Entity Account'
                    ],
            ]
        ]);

        $this->addDefinition([
            'name' => 'Directory.Mailable.Post.Model',
            'type' => 'object',
            'properties' => [
                'id' => [
                    'type' => 'string',
                    'enum' => ['required|string'],
                    'description' => 'Mailable id is required for POST'
                ],
                'longName' => [
                    'type' => 'string',
                    'enum' => ['required|string'],
                    'description' => 'LongName of the Mailable Entity Account'
                ],
                'owner' => [
                    'type' => 'string',
                    'enum' => ['required|string'],
                    'description' => 'Owner of the Mailable Entity Account'
                ],
                'status' =>
                    [
                        'type' => 'string',
                        'enum' => ['required|string'],
                        'description' => 'Status of the Mailable Entity Account'
                    ],
            ]
        ]);


        $this->addDefinition([
            'name' => 'Directory.Mailable.Put.Model',
            'type' => 'object',
            'properties' => [
                'longName' =>
                    [
                        'type' => 'string',
                        'description' => 'LongName of the Mailable Entity Account'
                    ],
                'owner' =>
                    [
                        'type' => 'string',
                        'description' => 'Owner of the Mailable Entity Account'
                    ],
                'status' =>
                    [
                        'type' => 'string',
                        'description' => 'Status of the Mailable Entity Account'
                    ],
            ]
        ]);
    }

    public function registerServices(): void
    {
        $this->addService([
            'name' => 'Directory\Mailable',
            'class' => 'MiamiOH\ProjectsDirectoryRest\Services\Mailable',
            'description' => 'Provides account mailable information',
            'set' => [
                'database' => ['type' => 'service', 'name' => 'APIDatabaseFactory'],
            ],
        ]);

    }

    public function registerResources(): void
    {
        $this->addResource([
            'action' => 'read',
            'name' => 'directory.v1.mailable.id',
            'description' => 'Get Mailable Account details',
            'pattern' => '/directory/v1/mailable/:id',
            'service' => 'Directory\Mailable',
            'params' => [
                'id' => ['description' => 'Enter Entity Account Id.'],
            ],
            'method' => 'getMailable',
            'returnType' => 'model',
            'tags' => ['Directory'],
            'middleware' => [
                'authorize' => [
                    'application' => 'Directory Service',
                    'module' => 'Mailable',
                    'key' => 'read'
                ],
                'authenticate' => [
                    [
                        'type' => 'token'
                    ],
                ],
            ],
            'responses' => [
                App::API_OK => [
                    'description' => 'Mailable Account Information',
                    'returns' => [
                        'type' => 'model',
                        'schema' => [
                            '$ref' => '#/definitions/Directory.Mailable.Model',
                        ]
                    ]
                ],
                App::API_NOTFOUND => [
                    'description' => 'Mailable Account not found',
                ]
            ]
        ]);

        $this->addResource([
            'action' => 'create',
            'name' => 'directory.v1.mailable.create.id',
            'description' => 'Create Mailable Account',
            'pattern' => '/directory/v1/mailable',
            'service' => 'Directory\Mailable',
            'method' => 'createMailable',
            'tags' => ['Directory'],
            'middleware' => [
                'authorize' => [
                    'application' => 'Directory Service',
                    'module' => 'Mailable',
                    'key' => 'create'
                ],
                'authenticate' => [
                    [
                        'type' => 'token'
                    ],
                ],
            ],
            'body' => [
                'description' => 'Entity Account information',
                'required' => true,
                'schema' => [
                    '$ref' => '#/definitions/Directory.Mailable.Post.Model'
                ]
            ],
            'responses' => [
                App::API_CREATED => [
                    'description' => 'Entity Account created successfully.',
                ],
                App::API_BADREQUEST => [
                    'description' => 'Some or all data are bad.',
                ],
                App::API_FAILED => [
                    'description' => 'Create operation failed.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],
            ],
        ]);

        $this->addResource([
            'action' => 'update',
            'name' => 'directory.v1.mailable.put.id',
            'description' => 'Update Mailable Account',
            'pattern' => '/directory/v1/mailable/:id',
            'params' => [
                'id' => ['description' => 'Enter Mailable Id.'],
            ],
            'service' => 'Directory\Mailable',
            'method' => 'updateMailable',
            'tags' => ['Directory'],
            'middleware' => [
                'authorize' => [
                    'application' => 'Directory Service',
                    'module' => 'Mailable',
                    'key' => 'update'
                ],
                'authenticate' => [
                    [
                        'type' => 'token'
                    ],
                ],
            ],
            'body' => [
                'description' => 'Entity Account information',
                'required' => true,
                'schema' => [
                    '$ref' => '#/definitions/Directory.Mailable.Put.Model'
                ]
            ],
            'responses' => [
                App::API_OK => [
                    'description' => 'Updated successfully.',
                ],
                App::API_BADREQUEST => [
                    'description' => 'Some or all data are bad.',
                ],
                App::API_FAILED => [
                    'description' => 'Update operation failed.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],
            ],
        ]);

        $this->addResource([
            'action' => 'delete',
            'name' => 'directory.v1.mailable.delete.id',
            'description' => 'Delete Mailable Account',
            'pattern' => '/directory/v1/mailable/:id',
            'service' => 'Directory\Mailable',
            'params' => [
                'id' => ['description' => 'Enter Entity Account Id.'],
            ],
            'method' => 'deleteMailable',
            'tags' => ['Directory'],
            'middleware' => [
                'authorize' => [
                    'application' => 'Directory Service',
                    'module' => 'Mailable',
                    'key' => 'delete'
                ],
                'authenticate' => [
                    [
                        'type' => 'token'
                    ],
                ],
            ],
            'responses' => [
                App::API_OK => [
                    'description' => 'Deleted successfully.',
                ],
                App::API_BADREQUEST => [
                    'description' => 'Some or all data are bad.',
                ],
                App::API_FAILED => [
                    'description' => 'Delete operation failed.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],
            ],
        ]);
    }

    public function registerOrmConnections(): void
    {

    }
}
