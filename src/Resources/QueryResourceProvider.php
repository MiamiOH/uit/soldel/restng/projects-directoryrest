<?php

namespace MiamiOH\ProjectsDirectoryRest\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class QueryResourceProvider extends ResourceProvider
{


    public function registerDefinitions(): void
    {
        $this->addTag(array(
            'name' => 'Directory',
            'description' => 'Resources for Directory Services'
        ));


        $this->addDefinition(array(
            'name' => 'Directory.Query.Entry',
            'type' => 'object',
            'properties' => array(
                'uid' => array('type' => 'string', 'description' => 'uniqueid'),
                'firstName' => array('type' => 'string', 'description' => 'First name of user'),
                'lastName' => array('type' => 'string', 'description' => 'Last name of user'),
                'fullName' => array('type' => 'string', 'description' => 'Full name of user'),
                'fullNameReverse' => array('type' => 'string', 'description' => 'Reversed full name of user'),
                'hiddenStatus' => array('type' => 'string', 'description' => 'LDAP hidden status of a user'),
                'affiliation' => array('type' => 'string', 'description' => 'Primary affiliation code'),
                'affiliationList' => array('type', 'list', 'description' => 'List of all affilication codes'),
            )
        ));

        $this->addDefinition(array(
            'name' => 'Directory.Query.Entry.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Directory.Query.Entry'
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'Directory\Query',
            'class' => 'MiamiOH\ProjectsDirectoryRest\Services\Query',
            'description' => 'Provides directory query functions',
            'set' => array(
                'ldapFactory' => array('type' => 'service', 'name' => 'APILDAPFactory'),
            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'directory.v1.query.mailLookup',
            'description' => 'Conduct mail lookup queries for UX support',
            'pattern' => '/directory/v1/query/mailLookup',
            'service' => 'Directory\Query',
            'method' => 'mailLookup',
            'options' => array(
                'q' => array('description' => 'string to lookup as a mail address'),
            ),
            'returnType' => 'model',
            'tags' => array('Directory'),
            'middleware' => array(
                'authenticate' => array(
                    array(
                        'type' => 'anonymous'
                    ),
                    array(
                        'type' => 'token'
                    )
                )
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'Query entry',
                    'returns' => array(
                        'type' => 'model',
                        'schema' => array(
                            '$ref' => '#/definitions/Directory.Query.Entry',
                        )
                    )
                ),
                App::API_NOTFOUND => array(
                    'description' => 'Query entry not found',
                )
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'directory.v1.query.lookup',
            'description' => 'Conduct directory lookups for UX support. Anonymous lookups will only return meaningful data for un-hidden users. Use an authorized token to get data for all users. ',
            'pattern' => '/directory/v1/query/lookup',
            'service' => 'Directory\Query',
            'method' => 'lookup',
            'options' => array(
                'q' => array('description' => 'string to lookup as an id'),
            ),
            'returnType' => 'model',
            'tags' => array('Directory'),
            'middleware' => array(
                'authenticate' => array(
                    array(
                        'type' => 'anonymous'
                    ),
                    array(
                        'type' => 'token'
                    )
                )
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'Query entry',
                    'returns' => array(
                        'type' => 'model',
                        'schema' => array(
                            '$ref' => '#/definitions/Directory.Query.Entry',
                        )
                    )
                ),
                App::API_NOTFOUND => array(
                    'description' => 'Query entry not found',
                )
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'directory.v1.query.typeahead',
            'description' => 'Conduct typeahead queries for UX support',
            'pattern' => '/directory/v1/query/typeahead',
            'service' => 'Directory\Query',
            'method' => 'typeahead',
            'options' => array(
                'q' => array('description' => 'string to use in the query'),
                'affiliation' => array('type' => 'list', 'description' => 'One or more affiliation codes to filter on'),
            ),
            'returnType' => 'collection',
            'tags' => array('Directory'),
            'middleware' => array(
                'authenticate' => array(
                    array(
                        'type' => 'anonymous'
                    ),
                    array(
                        'type' => 'token'
                    )
                )
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'Collection of query entries',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/Directory.Query.Entry.Collection',
                    )
                )
            )
        ));

    }

    public function registerOrmConnections(): void
    {

    }
}