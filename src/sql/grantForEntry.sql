grant select on szbuniq to muws_gen;
grant select on gzbeatt to muws_gen;
grant select on safmgr.mm4f_family_member_info to muws_gen;
grant select on gzvagd2 to muws_gen;
grant select, update on gzragst to muws_gen;
grant select, insert on gzbfmacc to muws_gen;
commit;