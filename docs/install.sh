#!/usr/bin/env bash

# RESTng integration

export DIRECTORYREST_HOME=/opt/webapps/wc/directoryREST

# Integrate with RESTng
# One time for new RESTng install
if [ ! -d "$RESTNG_HOME/RESTng/RESTconfig/directory" ]; then
    mkdir $RESTNG_HOME/RESTng/RESTconfig/directory
fi

if [ "$(ls -A $DIRECTORYREST_HOME/rest/src/RESTconfig)" ]; then
    for configName in $DIRECTORYREST_HOME/rest/src/RESTconfig/*; do
        linkName=$(basename "$configName");
        if [ ! -e "$RESTNG_HOME/RESTng/RESTconfig/directory/$linkName" ]; then
            echo "need to create $linkName";
            ln -s $configName $RESTNG_HOME/RESTng/RESTconfig/directory/$linkName;
        fi
    done
fi

if [ ! -d "$RESTNG_HOME/RESTng/Service/Directory" ]; then
    mkdir $RESTNG_HOME/RESTng/Service/Directory
fi

if [ "$(ls -A $DIRECTORYREST_HOME/rest/src/Service)" ]; then
    for serviceName in $DIRECTORYREST_HOME/rest/src/Service/*; do
        linkName=$(basename "$serviceName");
        if [ ! -e "$RESTNG_HOME/RESTng/Service/Directory/$linkName" ]; then
            echo "need to create $linkName";
            ln -s $serviceName $RESTNG_HOME/RESTng/Service/Directory/$linkName;
        fi
    done
fi
