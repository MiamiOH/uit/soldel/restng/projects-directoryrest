<?php

return [
    'resources' => [
        'directory' => [
            MiamiOH\ProjectsDirectoryRest\Resources\AliasResourceProvider::class,
            MiamiOH\ProjectsDirectoryRest\Resources\QueryResourceProvider::class,
            MiamiOH\ProjectsDirectoryRest\Resources\MailableResourceProvider::class,
            MiamiOH\ProjectsDirectoryRest\Resources\EntryResourceProvider::class,
        ],
    ]
];