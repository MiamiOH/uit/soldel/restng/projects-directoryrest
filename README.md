# DirectoryRest Service
## OpenLDAP Data Usage

This resource is set up to use production OpenLDAP directory. This is to avoid using outdated resources, like test OpenLDAP directory, when manipulating data via this service. For similar reasons, test CAS uses production OpenLDAP for authentication. Please keep this in mind when utilizing this resource to manage directories.

## Notes:

Team East converted this project to RESTng 2.0 on 08/07/2018.